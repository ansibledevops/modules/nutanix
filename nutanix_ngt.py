#!/usr/bin/python

# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: nutanix-ngt

short_description: Mount and Install NGT with VSS and Self Service Restore

version_added: "2.4"

description:
    - "Mount and install NGT to a VM and install it, plus VSS and Self Service Restore if requested
      "

options:
    name:
        description:
            - This is the message to send to the sample module
        required: true
    new:
        description:
            - Control to demo if the result of this module is changed or not
        required: false

extends_documentation_fragment:
    - azure

author:
    - Your Name (@yourhandle)
'''

EXAMPLES = '''
# Pass in a message
- name: Test with a message
  my_new_test_module:
    name: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  my_new_test_module:
    name: hello world
    new: true

# fail the module
- name: Test failure of the module
  my_new_test_module:
    name: fail me
'''

RETURN = '''
original_message:
    description: The original name param that was passed in
    type: str
message:
    description: The output message that the sample module generates
'''

from ansible.module_utils.basic import AnsibleModule
import requests


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        # Nutanix required params
        cluster=dict(type='str', required=True),
        api_username=dict(type='str', required=True),
        api_password=dict(type='str', required=True, no_log=True),
        vm_name=dict(type='str', required=True),
        options=dict(type='list', default=[]),  # VSS AND/OR SSR
        state=dict(type='str', default='present', choices=['present', 'absent'])
    )

    result = dict(
        changed=False,
        original_message='',
        message='',
        error='',
        data=''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        required_if=([('state', 'present', ['options'])]),
        supports_check_mode=True
    )

    changed = False
    message = ""

    api_auth = (module.params['api_username'], module.params['api_password'])

    api_url = "https://" + module.params['cluster'] + ":9440/api/nutanix/v3/vms/list"
    data = {
        "length": 100,
        "offset": 0,
        "filter": "vm_name==" + module.params['vm_name']
    }

    headers = {
        "content-type": "application/json"
    }

    api_response = requests.post(api_url, json=data, headers=headers, auth=api_auth, verify=False)

    check_response = api_response.json()

    if module.params['state'] == 'present':
        # If NGT not installed, need to install.
        # Check to see if NGT installed.

        # If installed, check to make sure the correct selected options installed.
        if 'guest_tools' not in check_response['entities'][0]['spec']['resources']:
            changed = True
            message = "NGT not installed, will install."
        else:
            if check_response['entities'][0]['spec']['resources']['guest_tools']['nutanix_guest_tools']['ngt_state'] != "INSTALLED":
                changed = True
                message += " NGT not installed, will install."

            for option in module.params['options']:
                if option not in check_response['entities'][0]['spec']['resources']['guest_tools']['nutanix_guest_tools']['enabled_capability_list']:
                    changed = True
                    message += "will install " + option

    elif module.params['state'] == 'absent':
        # If NGT installed, need to uninstall.
        if check_response['entities'][0]['spec']['resources']['guest_tools']['nutanix_guest_tools']['ngt_state'] == "INSTALLED":
            changed = True
            message = "Uninstalling NGT as requested."

    # if changed and not module.check_mode:
    #     if module.params['state'] == 'present':
    #
    #     elif module.params['state'] == 'absent':

    result['changed'] = changed
    result['message'] = message

    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()