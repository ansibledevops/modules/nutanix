#!/usr/bin/python

# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: nutanix-vm

short_description: Create and Destory VM's on a Nutanix Cluster

version_added: "2.4"

description:
    - "Create and Destory VM's on a Nutanix Cluster. Simply module where you can specify memory, cpu, disks and network settings
      "

options:
    name:
        description:
            - This is the message to send to the sample module
        required: true
    new:
        description:
            - Control to demo if the result of this module is changed or not
        required: false

author:
    - Martin Fedec
'''

EXAMPLES = '''
# Pass in a message
- name: Test with a message
  my_new_test_module:
    name: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  my_new_test_module:
    name: hello world
    new: true

# fail the module
- name: Test failure of the module
  my_new_test_module:
    name: fail me
'''

RETURN = '''
original_message:
    description: The original name param that was passed in
    type: str
message:
    description: The output message that the sample module generates
'''

from ansible.module_utils.basic import AnsibleModule
import requests


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        # Nutanix required params
        cluster=dict(type='str', required=True),
        api_username=dict(type='str', required=True),
        api_password=dict(type='str', required=True, no_log=True),
        vm_name=dict(type='str', required=True),
        vm_cpu=dict(type='int'),
        vm_mem_mib=dict(type='int'),
        vm_description=dict(type='str'),
        vm_power_state=dict(type='str', default="powered_off", choices=['powered_on', 'powered_off']),
        vm_disks=dict(type='list', default=[]),
        vm_networks=dict(type='list', default=[]),
        vm_guest_customization=dict(type='dict'),
        state=dict(type='str', default='present', choices=['present', 'absent'])
    )

    result = dict(
        changed=False,
        original_message='',
        message='',
        error='',
        data=''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        required_if=([('state', 'present', ['vm_cpu', 'vm_mem_mib'])]),
        supports_check_mode=True
    )

    changed = False
    vm_power_state = "OFF"

    api_auth = (module.params['api_username'], module.params['api_password'])

    api_url = "https://" + module.params['cluster'] + ":9440/api/nutanix/v3/vms/list"
    data = {
        "length": 100,
        "offset": 0,
        "filter": "vm_name==" + module.params['vm_name']
    }

    headers = {
        "content-type": "application/json"
    }

    api_response = requests.post(api_url, json=data, headers=headers, auth=api_auth, verify=False)

    check_response = api_response.json()

    if module.params['state'] == 'present':
        if check_response['metadata']['total_matches'] == 0:
            changed = True
    elif module.params['state'] == 'absent':
        if check_response['metadata']['total_matches'] == 1:
            changed = True

    if changed and not module.check_mode:
        if module.params['state'] == 'present':
            disk_details = []
            image_api_url = "https://" + module.params['cluster'] + ":9440/api/nutanix/v3/images/list"
            # Build disk list for API Call
            for disk in module.params['vm_disks']:
                if 'disk_image_name' in disk:
                    image_filter = {
                        "filter": "name==" + disk['disk_image_name']
                    }

                    image_api_response = requests.post(image_api_url, json=image_filter, headers=headers, auth=api_auth, verify=False)
                    image_api_response = image_api_response.json()

                    if image_api_response['metadata']['total_matches'] == 1:
                        disk_details.append({
                            "disk_size_mib": disk['disk_size_mib'],
                            "data_source_reference": {
                                "kind": "image",
                                "uuid": image_api_response['entities'][0]['metadata']['uuid']
                            }
                        })
                    else:
                        result['error'] = image_api_response
                        module.fail_json(msg='Could not find disk image, Failed to Create VM', **result)
                else:
                    disk_details.append({
                        "disk_size_mib": disk['disk_size_mib']
                    })


            # Query to get the UUID for the specified VLANs and build the network list to add to the VM
            network_details = []
            vlan_api_url = "https://" + module.params['cluster'] + ":9440/api/nutanix/v3/subnets/list"

            for net in module.params['vm_networks']:
                # Get the VLAN details
                vlan_filter = {
                    "filter": "vlan_id==" + str(net['vlan'])
                }
                vlan_api_response = requests.post(vlan_api_url, json=vlan_filter, headers=headers, auth=api_auth, verify=False)
                vlan_api_response = vlan_api_response.json()

                if vlan_api_response['metadata']['total_matches'] == 1:
                    network_details.append({
                        "subnet_reference": {
                            "kind": "subnet",
                            "uuid": vlan_api_response['entities'][0]['metadata']['uuid']
                        }
                    })
                else:
                    result['error'] = vlan_api_response
                    module.fail_json(msg='VM Creation Failed, Unable to find VLAN ID', **result)

            if module.params['vm_power_state'] == 'powered_on':
                vm_power_state = "ON"

            vm_details = {
                "metadata": {
                    "kind": "vm"
                },
                "spec": {
                    "description": module.params['vm_description'],
                    "resources": {
                        "num_sockets": module.params['vm_cpu'],
                        "memory_size_mib": module.params['vm_mem_mib'],
                        "power_state": vm_power_state,
                        "disk_list": disk_details,
                        "nic_list": network_details
                    },
                    "name": module.params['vm_name']
                }
            }

            # Guest customization section
            guest_cust_details = {}
            if module.params['vm_guest_customization']:
                if module.params['vm_guest_customization']['os'] == 'linux':
                    guest_cust_details = {
                        "cloud_init": {
                            "user_data": module.params['vm_guest_customization']['base64_data']
                        }
                    }
                elif module.params['vm_guest_customization']['os'] == 'win':
                    guest_cust_details = {
                        "sysprep": {
                            "unattend_xml": module.params['vm_guest_customization']['base64_data']
                        }
                    }

                vm_details['spec']['resources']['guest_customization'] = guest_cust_details

            api_url = "https://" + module.params['cluster'] + ":9440/api/nutanix/v3/vms"
            api_response = ""
            api_response = requests.post(api_url, json=vm_details, headers=headers, auth=api_auth, verify=False)
            if api_response.status_code == 202:
                result['message'] = "VM has been created"
                result['data'] = api_response.json()
            else:
                result['error'] = api_response.json()
                module.fail_json(msg='Failed to Create VM', **result)
        elif module.params['state'] == 'absent':
            api_url = "https://" + module.params['cluster'] + ":9440/api/nutanix/v3/vms/" + check_response['entities'][0]['metadata']['uuid']
            api_response = requests.delete(api_url, headers=headers, auth=api_auth, verify=False)
            if api_response.status_code == 202:
                result['message'] = "VM has been deleted"
            else:
                result['error'] = api_response.json()
                module.fail_json(msg='Failed to delete VM', **result)

    result['changed'] = changed

    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()